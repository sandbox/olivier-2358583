YIB is a small and light module.
This module allows to put a youtube video in a block.
You can create several blocks.

INTRODUCTION
------------

To manage the first block go to
/admin/structure/block/manage/youtube_in_block/1/configure

To add a new block go to
/admin/structure/block/add-yib
