<?php

/**
 * @file
 * Functions that are only called on the admin pages.
 */

/**
 * Add block form.
 */
function youtube_in_block_add_block_form($form, &$form_state) {
  module_load_include('inc', 'block', 'block.admin');
  $delta = variable_get('youtube_in_block_block_number', 1);
  return block_admin_configure($form, $form_state, 'youtube_in_block', ++$delta);
}

/**
 * Implements hook_form_submit().
 */
function youtube_in_block_add_block_form_submit($form, &$form_state) {
  variable_set('youtube_in_block_block_number', $form_state['values']['delta']);
  youtube_in_block_submit($form_state['values']['delta'], $form_state['values']);
  module_load_include('inc', 'block', 'block.admin');
  block_admin_configure_submit($form, $form_state);
}
